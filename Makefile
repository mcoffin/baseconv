.DEFAULT_GOAL := baseconv

LDFLAGS ?= -fuse-ld=gold -flto
CXXFLAGS ?= -O3 -fpic
CFLAGS ?= -O3 -fpic
CXXFLAGS += -std=c++20

c_sources=$(shell find src -type f -name '*.c')
cpp_sources=$(shell find src -type f -name '*.cpp')
objects=$(cpp_sources:.cpp=.o)
objects+=$(c_sources:.c=.o)

DEBUG ?=

ifneq ($(DEBUG),)
	CXXFLAGS += -DDEBUG=1 -g
endif

src/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

src/%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@

baseconv: $(objects)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

clean:
	$(foreach f,$(objects),[ ! -e "$(f)" ] || rm "$(f)";)
	[ ! -e baseconv ] || rm baseconv

.PHONY: clean
