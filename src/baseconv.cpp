#include "baseconv.hpp"
#include <iostream>
#include <sstream>
#include <cstdlib>

LongBaseConverter::LongBaseConverter(int inputBase, int outputBase):
	BaseConverter(inputBase, 10),
	format(std::dec)
{
	setOutputBase(outputBase);
}

void LongBaseConverter::setOutputBase(int obase)
{
	switch (obase) {
		case 10:
			format = std::dec;
			break;
		case 16:
			format = std::hex;
			break;
		case 8:
			format = std::hex;
			break;
		default:
			throw obase;
	}
	m_outputBase = obase;
}

long LongBaseConverter::parse(const char *s) {
	char *end = nullptr;
	auto ret = std::strtol(s, &end, inputBase());
	if (end == s) {
		throw std::string(s);
	}
	return ret;
}
