#include "baseconv.hpp"
#include "parse-base.h"
#include <iostream>
#include <ctype.h>
#include <cstdlib>
#include <unistd.h>
#include <getopt.h>

static LongBaseConverter createConverter(int argc, char *argv[])
{
	int inputBase = 16;
	int outputBase = 10;
	int opt;
	opterr = 0;
	while ((opt = getopt(argc, argv, "i:o:")) != -1) {
		switch (opt) {
			case 'i':
				inputBase = parseBase(optarg);
				break;
			case 'o':
				outputBase = parseBase(optarg);
				break;
			case '?':
				std::cerr << "Invalid argument: -" << optopt << std::endl;
				throw EXIT_FAILURE;
		}
	}
	return LongBaseConverter(inputBase, outputBase);
}

static void doConversion(LongBaseConverter& converter, const char *s) {
	std::cout << converter.format << converter.parse(s) << std::endl;
}

int main(int argc, char *argv[])
{
	LongBaseConverter converter(16, 10);
	try {
		converter = createConverter(argc, argv);
	} catch(int e) {
		return e;
	}
#ifdef DEBUG
	std::cerr << "Converting from base " << converter.inputBase() << " to " << converter.outputBase() << std::endl;
#endif
	if (argc - optind >= 1) {
#ifdef DEBUG
		std::cerr << "Taking input from command line." << std::endl;
#endif
		for (size_t i = optind; i < static_cast<size_t>(argc); i++) {
			doConversion(converter, argv[i]);
		}
	} else {
#ifdef DEBUG
		std::cerr << "Taking input from stdin." << std::endl;
#endif
		std::string s;
		while (std::cin >> s) {
			doConversion(converter, s.c_str());
		}
	}
	return EXIT_SUCCESS;
}
