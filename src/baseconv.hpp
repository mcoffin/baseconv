#pragma once

#include <string>
#include <ios>

template<typename T>
class BaseConverter {
	public:
		BaseConverter(int inputBase, int outputBase):
			m_inputBase(inputBase),
			m_outputBase(outputBase)
		{
		}

		inline int inputBase() {
			return m_inputBase;
		}

		inline int outputBase() {
			return m_outputBase;
		}

		int m_inputBase;
		int m_outputBase;
};

class LongBaseConverter: public BaseConverter<long> {
	public:
		LongBaseConverter(int inputBase, int outputBase);
		long parse(const char *s);
		std::ios_base& (*format)(std::ios_base&);
		void setOutputBase(int outputBase);
		std::ios_base& operator<<(std::string& s);
};
