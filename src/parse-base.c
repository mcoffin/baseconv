#include "parse-base.h"
#include <string.h>
#include <stdlib.h>

int parseBase(const char *base)
{
	if (strcmp(base, "hex") == 0 || strcmp(base, "hexidecimal") == 0) {
		return 16;
	}
	if (strcmp(base, "dec") == 0 || strcmp(base, "decimal") == 0) {
		return 10;
	}
	if (strcmp(base, "oct") == 0 || strcmp(base, "octal") == 0) {
		return 8;
	}

	// If we get to here, fall back to parsing as an int
	return atoi(base);
}
